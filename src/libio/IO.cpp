/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2012, 2013  Rouven Spreckels  <nevuor@nevux.org>            *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _IO_
#define _IO_

#include "IO.h"

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <string>
#include <sstream>

IO::IOS IO::ios;

IO::Data::Data(const void* data, Size size) {
	this->data = new char[size];
	for (Size i = 0; i < size; ++i)
		this->data[i] = *((const char*)data+i);
	this->size = size;
}

IO::Error::Error(std::string message, IO* io, int code) {
	this->message = message.c_str();
	this->io = io;
	this->code = code;
	error = errno;
}

const char* IO::Error::what() const throw() {
	return error ? ((std::string)message+": "+strerror(error)).c_str() : message;
}

IO::Timeout::Timeout(int sec, int usec) {
	this->sec = sec;
	this->usec = usec;
}

const char* IO::Timeout::what() const throw() {
	return ((std::string)
		"Timeout after "+
		((std::ostringstream&)(std::ostringstream() << sec)).str()+"s"+
		(usec ? " "+((std::ostringstream&)(std::ostringstream() << usec)).str()+"us" : "")
	).c_str();
}

void IO::onReadable() {
	if (toRead) {
		Size read = ::read(fd, destination, toRead);
		if (-1 == read)
			throw new Error("Cannot read", this, 1);
		toRead -= read;
		destination += read;
	}
	if (!toRead)
		select &= ~readable;
}

void IO::onWritable() {
	if (toWrite) {
		Size written = ::write(fd, source, toWrite);
		if (-1 == written)
			throw new Error("Cannot write", this, 2);
		toWrite -= written;
		source += written;
	}
	else {
		if (os.size()) {
			if (source == os[0].data+os[0].size)
				os.erase(os.begin());
			if (os.size())
				put(os[0].data, os[0].size);
		}
		else {
			select &= ~writable;
		}
	}
}

void IO::onEspecial() {
	throw new Error("Something espacial happend", this, 3);
}

IO::IO() {
	fd = 0;
	select = 0;
	toRead = toWrite = 0;
	source = destination = 0;
	ios.push_back(this);
}

IO::~IO() {
	for (IOS::iterator io = ios.begin(); io != ios.end(); ++io) {
		if ((*io)->fd == fd) {
			ios.erase(io);
			break;
		}
	}
	::close(fd);
}

bool IO::process(int sec, int usec) {
	timeval tv, *wait = 0;
	if (sec > -1 and !(sec == 0 and usec == 0)) {
		wait = &tv;
		wait->tv_sec  = sec;
		wait->tv_usec = usec;
	}
	fd_set readable, writable, especial;
	int max = -1;
	FD_ZERO(&readable);
	FD_ZERO(&writable);
	FD_ZERO(&especial);
	for (size_t i = 0; i < ios.size(); ++i) {
		if (ios[i]->select & (IO::readable | IO::writable | IO::especial)) {
			if (ios[i]->select & IO::readable)
				FD_SET(ios[i]->fd, &readable);
			if (ios[i]->select & IO::writable)
				FD_SET(ios[i]->fd, &writable);
			if (ios[i]->select & IO::especial)
				FD_SET(ios[i]->fd, &especial);
			if (max < ios[i]->fd)
				max = ios[i]->fd;
		}
	}
	if (-1 == max)
		return false;
	switch (::select(max+1, &readable, &writable, &especial, wait)) {
		case -1:
			if (errno != EINTR)
				throw new Error("Cannot select", 0, 4);
		break;
		case 0:
			throw new Timeout(sec, usec);
		break;
	}
	for (size_t i = 0; i < ios.size(); ++i) {
		if (FD_ISSET(ios[i]->fd, &readable))
			ios[i]->onReadable();
		if (FD_ISSET(ios[i]->fd, &writable))
			ios[i]->onWritable();
		if (FD_ISSET(ios[i]->fd, &especial))
			ios[i]->onEspecial();
	}
	return true;
}

void IO::get(void* data, Size size) {
	if (!toRead) {
		toRead = size;
		destination = (char*)data;
		select |= readable;
	}
	else
		throw new Error("Already reading", this, 5);
}

void IO::put(const void* data, Size size) {
	if (toWrite) {
		os.push_back(Data(data, size));
	}
	else {
		toWrite = size;
		source = (const char*)data;
		select |= writable;
	}
}

IO::Size IO::hasToRead() {
	return toRead;
}

IO::Size IO::hasToWrite() {
	return toWrite;
}

IO::Size IO::hasToPut() {
	return os.size();
}

void IO::close() {
	if (-1 == ::close(fd))
		throw new Error("Cannot close", this, 6);
}

#endif
