/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2012, 2013  Rouven Spreckels  <nevuor@nevux.org>            *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _IO_H
#define _IO_H

#include <vector>
#include <exception>
#include <string>
#include <unistd.h>

class IO {
protected:
	typedef std::vector<IO*> IOS;
	static IOS ios;
	int fd;
	typedef char State;
	State select;
	static const State
	readable = 1,
	writable = 2,
	especial = 4;
	const char* source;
	char* destination;
	typedef ssize_t Size;
	Size toRead, toWrite;
	class Data {
	public:
		char* data;
		Size size;
		Data(const void* data, Size size);
	};
	typedef std::vector<Data> OS;
	OS os;
	IO();
	~IO();
	virtual void onReadable();
	virtual void onWritable();
	virtual void onEspecial();
public:
	class Error : public std::exception {
	public:
		const char* message;
		IO* io;
		int code;
		int error;
		Error(std::string message, IO* io = 0, int code = 0);
		virtual const char* what() const throw();
	};
	class Timeout : public std::exception {
	public:
		int sec, usec;
		Timeout(int sec, int usec);
		virtual const char* what() const throw();
	};
	static bool process(int sec = -1, int usec = 0);
	void get(void* data, Size size);
	void put(const void* data, Size size);
	Size hasToRead();
	Size hasToWrite();
	Size hasToPut();
	void close();
};

#endif
