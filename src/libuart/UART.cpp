/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2012, 2013  Rouven Spreckels  <nevuor@nevux.org>            *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _UART_
#define _UART_

#include "UART.h"

#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <string>
#include <sstream>

const int UART::BAUD = 9600;
const char* UART::MODE = "8N1";

UART::UART() {
}

UART::UART(Device device, Baud baud, Mode mode)
: IO() {
	setup(device, baud, mode);
	open();
}

void UART::setup(Device device, Baud baud, Mode mode) {
	this->device = device;
	switch (baud) {
		case       0: this->baud =       B0; break;
		case      50: this->baud =      B50; break;
		case      75: this->baud =      B75; break;
		case     110: this->baud =     B110; break;
		case     134: this->baud =     B134; break;
		case     150: this->baud =     B150; break;
		case     200: this->baud =     B200; break;
		case     300: this->baud =     B300; break;
		case     600: this->baud =     B600; break;
		case    1200: this->baud =    B1200; break;
		case    1800: this->baud =    B1800; break;
		case    2400: this->baud =    B2400; break;
		case    4800: this->baud =    B4800; break;
		case    9600: this->baud =    B9600; break;
		case   19200: this->baud =   B19200; break;
		case   38400: this->baud =   B38400; break;
		case   57600: this->baud =   B57600; break;
		case  115200: this->baud =  B115200; break;
		case  230400: this->baud =  B230400; break;
		case  460800: this->baud =  B460800; break;
		case  500000: this->baud =  B500000; break;
		case  576000: this->baud =  B576000; break;
		case  921600: this->baud =  B921600; break;
		case 1000000: this->baud = B1000000; break;
		case      -1:
			this->baud = BAUD;
		break;
		default:
			throw new Error(
				(std::string)"Invalid baud "+
				((std::ostringstream&)(std::ostringstream() << baud)).str(),
				this, 1
			);
		break;
	}
	this->options = 0;
	if (strlen(mode)) {
		if (strlen(mode) != 3)
			throw new Error((std::string)"Invalid mode '"+mode+"'", this, 1);
		else
			strcpy(this->mode, mode);
	}
	else
		strcpy(this->mode, MODE);
	switch (this->mode[0]) {
		case '5': this->options |= CS5; break;
		case '6': this->options |= CS6; break;
		case '7': this->options |= CS7; break;
		case '8': this->options |= CS8; break;
	}
	switch (this->mode[1]) {
		case 'N': case 'n': break;
		case 'O': case 'o': this->options |= PARENB | PARODD; break;
		case 'E': case 'e': this->options |= PARENB; break;
	}
	switch (this->mode[2]) {
		case '1': break;
		case '2': this->options |= CSTOPB; break;
	}
}

void UART::open() {
	memset(&cfg, 0, sizeof cfg);
        cfg.c_cflag = CREAD | CLOCAL | options;
	if (-1 == cfsetospeed(&cfg, baud))
		throw new Error("Cannot set output speed", this, 2);
	if (-1 == cfsetispeed(&cfg, baud))
		throw new Error("Cannot set input speed", this, 3);
	if (-1 == (fd = ::open(device, O_RDWR | O_NOCTTY | O_NDELAY)))
		throw new Error((std::string)"Cannot open '"+device+"'", this, 4);
	if (-1 == tcsetattr(fd, TCSANOW, &cfg))
		throw new Error("Cannot set terminal parameters", this, 5);
}

#endif
