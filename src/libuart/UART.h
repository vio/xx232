/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2012, 2013  Rouven Spreckels  <nevuor@nevux.org>            *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _UART_H
#define _UART_H

#include "../libio/IO.h"

#include <termios.h>

class UART : public IO {
public:
	typedef const char* Device;
	typedef int Baud;
	typedef const char Mode[4];
	static const int BAUD;
	static const char* MODE;
private:
	struct termios cfg;
	Device device;
	Baud baud;
	char mode[4];
	int options;
public:
	UART();
	UART(Device device, Baud baud = -1, Mode mode = "");
	void setup(Device device, Baud baud = -1, Mode mode = "");
	void open();
};

#endif
