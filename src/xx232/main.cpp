/******************************************************************************
 *  This file is part of xx232.                                               *
 *                                                                            *
 *  Copyright (C) 2012, 2013  Rouven Spreckels  <nevuor@nevux.org>            *
 *                                                                            *
 *  xx232 is free software: you can redistribute it and/or modify             *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  xx232 is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with xx232.  If not, see <http://www.gnu.org/licenses/>.            *
 ******************************************************************************/

#define NAME      "xx232"
#define VERSION   "1.0.0"
#define COPYRIGHT "Copyright (C) 2012, 2013  Rouven Spreckels  <nevuor@nevux.org>"

#define DEFAULT_RATE   UART::BAUD
#define DEFAULT_MODE   UART::MODE
#define DEFAULT_BASE   "str"
#define DEFAULT_SIZE   0
#define DEFAULT_TIME_S 3
#define DEFAULT_TIME_U 0

#include "../libuart/UART.h"

#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <bitset>
#include <sstream>
#include <iomanip>

void usage(const char* name) {
	std::cout << "usage: " << name << " [option <value>]... <device> <tx [tx]...>" << std::endl;
	std::cout << "-r, --rate <rate>        Rate like 115200; default is " << DEFAULT_RATE << std::endl;
	std::cout << "-m, --mode <mode>        Mode like 8E2 or 5O1; default is " << DEFAULT_MODE << std::endl;
	std::cout << "-b, --base <base>        Base like str, bin, oct, dec, or hex; default is " << DEFAULT_BASE << std::endl;
	std::cout << "-s, --size <size>        Size of rx like -1 (inf), 0 (msg), or 5; default is " << DEFAULT_SIZE << std::endl;
	std::cout << "-t, --time <sec> <usec>  Time like 0 0 (inf), or 2 500000; default is " << DEFAULT_TIME_S << " " << DEFAULT_TIME_U << std::endl;
}

void version() {
	std::cout << NAME << "-" << VERSION << std::endl;
	std::cout << COPYRIGHT << std::endl;
}

int main(int argc, char** argv) {
	const char* name = argc > 0 ? argv[0] : NAME;
	typedef std::vector<std::string> Args;
	Args args;
	for (int i = 1; i < argc; ++i)
		args.push_back((std::string)argv[i]);
	if (!args.size()) {
		usage(name);
		return 0;
	}
	if (args[0] == "-h" or args[0] == "--help") {
		usage(name);
		return 0;
	}
	else
	if (args[0] == "-v" or args[0] == "--version") {
		version();
		return 0;
	}
	int rate = DEFAULT_RATE, size = DEFAULT_SIZE, time[2] = {DEFAULT_TIME_S, DEFAULT_TIME_U};
	std::string mode = DEFAULT_MODE, base = DEFAULT_BASE;
	size_t i;
	for (i = 0; i < args.size()-2; ++i) {
		if (args[i] == "-r" or args[i] == "--rate") {
			if (!(i+1 < args.size())) {
				usage(name);
				return 1;
			}
			std::istringstream(args[++i]) >> rate;
		}
		else
		if (args[i] == "-m" or args[i] == "--mode") {
			if (!(i+1 < args.size())) {
				usage(name);
				return 2;
			}
			mode = args[++i];
		}
		else
		if (args[i] == "-b" or args[i] == "--base") {
			if (!(i+1 < args.size())) {
				usage(name);
				return 3;
			}
			base = args[++i];
		}
		else
		if (args[i] == "-s" or args[i] == "--size") {
			if (!(i+1 < args.size())) {
				usage(name);
				return 4;
			}
			std::istringstream(args[++i]) >> size;
		}
		else
		if (args[i] == "-t" or args[i] == "--time") {
			if (!(i+2 < args.size())) {
				usage(name);
				return 5;
			}
			std::istringstream(args[++i]) >> time[0];
			std::istringstream(args[++i]) >> time[1];
		}
		else {
			break;
		}
	}
	if (i != args.size()-2) {
		usage(name);
		return 6;
	}
	std::string device = args[i], message = args[i+1];
	if (base != "str") {
		int radix;
		if (base == "bin")
			radix = 2;
		else
		if (base == "oct")
			radix = 8;
		else
		if (base == "dec")
			radix = 10;
		else
		if (base == "hex")
			radix = 16;
		else {
			usage(name);
			return 7;
		}
		char* begin = (char*)message.c_str();
		std::string result;
		while (begin) {
			char* end, tx = strtol(begin, &end, radix);
			if (begin != end)
				result.append(1, tx);
			else
				end = 0;
			begin = end;
		}
		message = result;
	}
	bool flushed = false;
	try {
		UART si(device.c_str(), rate, mode.c_str());
		si.put(message.c_str(), message.size());
		unsigned char rx;
		si.get(&rx, 1);
		for (int i = 0; IO::process(time[0], time[1]);) {
			if (!si.hasToRead()) {
				if (base != "str" and flushed)
					std::cout << " " << std::flush;
				int bits;
				std::istringstream(mode.substr(0, 1)) >> bits;
				if (base == "bin") {
					switch (bits) {
						case 5: std::cout << (std::bitset<5>)rx << std::flush; break;
						case 6: std::cout << (std::bitset<6>)rx << std::flush; break;
						case 7: std::cout << (std::bitset<7>)rx << std::flush; break;
						case 8: std::cout << (std::bitset<8>)rx << std::flush; break;
					}
				}
				else
				if (base == "oct")
					std::cout << std::setw(bits > 6 ? 3 : 2) << std::setfill('0') << std::oct << (int)rx << std::flush;
				else
				if (base == "dec")
					std::cout << std::setw(bits > 6 ? 3 : 2) << std::setfill('0') << std::dec << (int)rx << std::flush;
				else
				if (base == "hex")
					std::cout << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)rx << std::flush;
				else
					std::cout << rx << std::flush;
				flushed = true;
				if (size == -1 or (size == 0 and ++i < (int)message.size()) or (size > 0 and ++i < size))
					si.get(&rx, 1);
			}
		}
	}
	catch (IO::Timeout* timeout) {
		if (flushed)
			std::cout << std::endl;
		std::cerr << timeout->what() << std::endl;
		return 1;
	}
	catch (IO::Error* error) {
		if (flushed)
			std::cout << std::endl;
		std::cerr << error->what() << std::endl;
		return error->code;
	}
	if (flushed)
		std::cout << std::endl;
	return 0;
}
